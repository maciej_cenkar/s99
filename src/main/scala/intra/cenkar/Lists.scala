package intra.cenkar

import java.util.concurrent.ThreadLocalRandom

import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.Random

object Lists {

  //  P01 (*) Find the last element of a list.
  @tailrec
  def last[A](list: List[A]): A = list match {
    case head :: tail => if (tail.isEmpty) head else last(tail)
    case Nil => throw new IllegalArgumentException
  }

  //  P02 (*) Find the last but one element of a list.
  @tailrec
  def penultimate[A](list: List[A]): A = list match {
    case Nil => throw new IllegalArgumentException
    case xs :: Nil => throw new IllegalArgumentException
    case xs :: ys :: tail => if (tail.isEmpty) xs else penultimate(ys :: tail)
  }

  //  P03 (*) Find the Kth element of a list.
  @tailrec
  def nth[A](n: Int, list: List[A]): A = {
    require(n >= 0)
    require(!list.isEmpty)
    if (n == 0) list.head else nth(n - 1, list.tail)
  }

  //  P04 (*) Find the number of elements of a list.
  def length(list: List[Any]): Int = list match {
    case xs :: tail => 1 + length(tail)
    case Nil => 0
  }

  def lengthtr(list: List[Any]): Int = {
    @tailrec
    def lengthtr(list: List[Any], size: Int): Int = list match {
      case xs :: tail => lengthtr(tail, size + 1)
      case Nil => size
    }
    lengthtr(list, 0)
  }

  //  P05 (*) Reverse a list.
  def reverse[A](list: List[A]): List[A] = {
    @tailrec
    def reverse[A](list: List[A], result: List[A]): List[A] = list match {
      case Nil => result
      case xs :: tail => reverse(tail, xs :: result)
    }
    reverse(list, Nil)
  }

  //  P06 (*) Find out whether a list is a palindrome.
  def isPalindrome(list: List[Any]): Boolean = reverse(list) == list

  //  P07 (**) Flatten a nested list structure.
  def flatten(list: List[Any]): List[Any] = list match {
    case (xs: List[Any]) :: tail => flatten(xs) ::: flatten(tail)
    case xs :: tail => xs :: flatten(tail)
    case Nil => Nil
  }

  def flattentr(list: List[Any]): List[Any] = ???

  //  P08 (**) Eliminate consecutive duplicates of list elements.
  def compress[A](list: List[A]): List[A] = list match {
    case Nil => Nil
    case xs :: tail => xs :: compress(dropWhile(tail, xs))
  }

  def compresstr[A](list: List[A]): List[A] = ???

  @tailrec
  def dropWhile[A](list: List[A], droppedElement: A): List[A] = list match {
    case xs :: tail => if (xs == droppedElement) dropWhile(tail, droppedElement) else xs :: tail
    case Nil => Nil
  }

  //  P09 (**) Pack consecutive duplicates of list elements into sublists.
  def pack[A](list: List[A]): List[List[A]] = list match {
    case xs :: tail => {
      val (left, right): (List[A], List[A]) = tail.span(_ == xs)
      (xs :: left) :: pack(right)
    }
    case Nil => Nil
  }

  def packtr[A](list: List[A]): List[List[A]] = ???

  def span[A](list: List[A], splittingElement: A): (List[A], List[A]) = ???

  //  P10 (*) Run-length encoding of a list.
  def encode[A](list: List[A]): List[(Int, A)] = pack(list) map (x => (length(x), x.head))

  def map[A, B](list: List[A], fun: A => B): List[B] = ???

  //  P11 (*) Modified run-length encoding.
  def encodeModified[A](list: List[A]): List[Any] = pack(list) map {
    case xs :: Nil => xs
    case xs :: tail => (length(tail) + 1, xs)
    case Nil => throw new IllegalArgumentException
  }

  //  P12 (**) Decode a run-length encoded list.
  def decode[A](list: List[(Int, A)]): List[A] = (list map { case (count, element) => List.fill(count)(element)}).flatten

  def fill[A](element: A, count: Int): List[A] = ???

  //  P13 (**) Run-length encoding of a list (direct solution).
  def encodeDirect[A](list: List[A]): List[(Int, A)] = {
    @tailrec
    def encodeDirect[A](list: List[A], currentElement: A, count: Int, acc: List[(Int, A)]): List[(Int, A)] = list match {
      case xs :: tail if (xs == currentElement) => encodeDirect(tail, currentElement, count + 1, acc)
      case xs :: tail if (xs != currentElement) => encodeDirect(tail, xs, 1, (count, currentElement) :: acc)
      case Nil => reverse((count, currentElement) :: acc)
    }
    list match {
      case xs :: tail => encodeDirect(tail, xs, 1, Nil)
      case Nil => Nil
    }
  }

  //  P14 (*) Duplicate the elements of a list.
  def duplicate[A](list: List[A]): List[A] = {
    @tailrec
    def duplicate[A](list: List[A], acc: List[A]): List[A] = list match {
      case xs :: tail => duplicate(tail, xs :: xs :: acc)
      case Nil => acc
    }
    reverse(duplicate(list, Nil))
  }

  //  P15 (**) Duplicate the elements of a list a given number of times.
  def duplicateN[A](count: Int, list: List[A]): List[A] = {
    @tailrec
    def duplicateN[A](list: List[A], acc: List[A]): List[A] = list match {
      case xs :: tail => duplicateN(tail, List.fill(count)(xs) ::: acc)
      case Nil => acc
    }
    reverse(duplicateN(list, Nil))
  }

  //  P16 (**) Drop every Nth element from a list.
  def drop[A](n: Int, list: List[A]): List[A] = {
    @tailrec
    def drop[A](list: List[A], acc: List[A], count: Int): List[A] = list match {
      case xs :: tail if (count == 1) => drop(tail, acc, n)
      case xs :: tail => drop(tail, xs :: acc, count - 1)
      case Nil => acc
    }
    reverse(drop(list, Nil, n))
  }

  //  P17 (*) Split a list into two parts.
  def split[A](count: Int, list: List[A]): (List[A], List[A]) = {
    @tailrec
    def split[A](list: List[A], firstPart: List[A], remaining: Int): (List[A], List[A]) = list match {
      case xs :: tail if (remaining == 0) => (reverse(firstPart), xs :: tail)
      case xs :: tail => split(tail, xs :: firstPart, remaining - 1)
      case Nil if (remaining == 0) => (reverse(firstPart), Nil)
      case Nil => throw new IllegalArgumentException
    }
    split(list, Nil, count)
  }

  //  P18 (**) Extract a slice from a list.
  def slice[A](from: Int, to: Int, list: List[A]): List[A] = {
    @tailrec
    def drop[A](count: Int, list: List[A]): List[A] = list match {
      case xs :: tail if (count == 0) => xs :: tail
      case xs :: tail => drop(count - 1, tail)
      case Nil => if (count == 0) Nil else throw new IllegalArgumentException
    }
    @tailrec
    def take[A](count: Int, list: List[A], acc: List[A]): List[A] = list match {
      case xs :: tail if (count == 0) => reverse(acc)
      case xs :: tail => take(count - 1, tail, xs :: acc)
      case Nil => if (count == 0) reverse(acc) else throw new IllegalArgumentException
    }
    require(to >= from)
    require(from >= 0)
    take(to - from, drop(from, list), Nil)
  }

  //  P19 (**) Rotate a list N places to the left.
  def rotate[A](count: Int, list: List[A]): List[A] = {
    val splitPoint = if (count >= 0) count else length(list) + count
    val (left, right) = split(splitPoint, list)
    right ::: left
  }

  //  P20 (*) Remove the Kth element from a list.
  def removeAt[A](index: Int, list: List[A]): (List[A], A) = {
    @tailrec
    def removeAt[A](distance: Int, list: List[A], firstPart: List[A]): (List[A], A) = list match {
      case xs :: tail if (distance == 0) => (reverse(firstPart) ::: tail, xs)
      case xs :: tail => removeAt(distance - 1, tail, xs :: firstPart)
      case Nil => throw new IllegalArgumentException
    }
    require(index >= 0)
    removeAt(index, list, Nil)
  }

  //  P21 (*) Insert an element at a given position into a list.
  def insertAt[A](element: A, index: Int, list: List[A]): List[A] = {
    @tailrec
    def insertAt[A](distance: Int, list: List[A], firstPart: List[A]): List[A] = list match {
      case xs :: tail if (distance == 0) => reverse(firstPart) ::: element.asInstanceOf[A] :: xs :: tail
      case xs :: tail => insertAt(distance - 1, tail, xs :: firstPart)
      case Nil => if (distance == 0) reverse(element.asInstanceOf[A] :: firstPart) else throw new IllegalArgumentException
    }
    require(index >= 0)
    insertAt(index, list, Nil)
  }

  //  P22 (*) Create a list containing all integers within a given range.
  def range(from: Int, to: Int): List[Int] = {
    @tailrec
    def range(from: Int, acc: List[Int]): List[Int] = if (from == to) reverse(from :: acc) else range(from + 1, from :: acc)
    require(to >= from)
    range(from, Nil)
  }

  //  P23 (**) Extract a given number of randomly selected elements from a list.
  def randomSelect[A](count: Int, list: List[A]): List[A] = {
    var remaining = count
    var listSize = length(list)
    require(count >= 0 && count <= listSize)
    var remainingList = list
    var result: List[A] = Nil
    while (remaining > 0) {
      val index: Int = Random.nextInt(listSize)
      val (afterRemoval, removed) = removeAt(index, remainingList)
      listSize = listSize - 1
      remaining = remaining - 1
      remainingList = afterRemoval
      result = removed :: result
    }
    result
  }

  def randomSelect2[A](count: Int, list: List[A]): List[A] = {
    val indices: Set[Int] = lotto(count, length(list))
    list.zipWithIndex filter { case (elem, index) => indices.contains(index + 1)} map (_._1)
  }

  //  P24 (*) Lotto: Draw N different random numbers from the set 1..M.
  def lotto(count: Int, end: Int): Set[Int] = {
    require(count >= 0)
    require(count <= end)
    require(end > 0)
    val random = ThreadLocalRandom.current()
    val set: mutable.Set[Int] = mutable.Set()
    while (set.size < count) {
      set.add(random.nextInt(end) + 1)
    }
    set.toSet
  }

  def lotto2(count: Int, end: Int): Set[Int] = {
    // for cases where count ~ end
    require(count >= 0)
    require(count <= end)
    require(end > 0)
    val random = ThreadLocalRandom.current()
    var remaining = count
    val buffer: mutable.Buffer[Int] = (1 to end).toBuffer
    val result: mutable.Set[Int] = mutable.Set()
    while (remaining > 0) {
      val removedIndex: Int = random.nextInt(buffer.size)
      result add buffer(removedIndex)
      buffer(removedIndex) = buffer.last
      buffer.remove(buffer.length - 1)
      remaining = remaining - 1
    }
    result.toSet
  }

  //  P25 (*) Generate a random permutation of the elements of a list.
  def randomPermute[A](list: List[A]): List[A] = randomSelect(length(list), list)

  //  P26 (**) Generate the combinations of K distinct objects chosen from the N elements of a list.
  def combinations[A](count: Int, list: List[A]): List[List[A]] = {
    // http://stackoverflow.com/questions/127704/algorithm-to-return-all-combinations-of-k-elements-from-n/127898#127898
    def combinations[A](count: Int, list: List[A], acc: List[A]): List[List[A]] = list match {
      case xs :: tail if (count > 1) => combinations(count - 1, tail, xs :: acc) ::: combinations(count, tail, acc)
      case xs if (count == 1) => xs map { x => reverse(x :: acc)}
      case Nil => Nil
    }
    combinations(count, list, Nil)
  }

  def combinations2[A](count: Int, list: List[A]): List[List[A]] = list match {
    case x if (count == 1) => list map (List(_))
    case xs :: tail if (count > 1) => ((combinations(count - 1, tail)) map (xs :: _)) ::: (combinations(count, tail))
    case Nil => Nil
  }

  //  P27 (**) Group the elements of a set into disjoint subsets.
  def group3[A](list: List[A]): List[List[List[A]]] = {
    @tailrec
    def removeElements[A](list: List[A], elementsToRemove: Set[A], acc: List[A]): List[A] = list match {
      case xs :: tail if (elementsToRemove contains xs) => removeElements(tail, elementsToRemove, acc)
      case xs :: tail => removeElements(tail, elementsToRemove, xs :: acc)
      case Nil => reverse(acc)
    }
    for {
      quadruplet <- combinations(4, list)
      triplet <- combinations(3, removeElements(list, quadruplet.toSet, Nil))
      pair <- combinations(2, removeElements(list, quadruplet.toSet ++ triplet.toSet, Nil))
    } yield List(quadruplet, triplet, pair)
  }

  def group[A](sizes: List[Int], list: List[A]): List[List[List[A]]] = ???

  //  P28 (**) Sorting a list of lists according to length of sublists.
  def lsort[A](list: List[List[A]]): List[List[A]] = list sortWith { case (left, right) => length(left) < length(right)}

  def lsortFreq[A](list: List[List[A]]): List[List[A]] = {
    val frequencies = list map (_.length) groupBy (x => x) transform { case (key, value) => value.length}
    list.sortWith { case (left, right) => frequencies(left.length) < frequencies(right.length)}
  }

}
