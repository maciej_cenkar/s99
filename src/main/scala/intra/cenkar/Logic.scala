package intra.cenkar

import scala.annotation.tailrec

object Logic {

  //  P46 (**) Truth tables for logical expressions.
  def and(a: Boolean, b: Boolean): Boolean = a && b

  def or(a: Boolean, b: Boolean): Boolean = a || b

  def not(a: Boolean): Boolean = !a

  def nand(a: Boolean, b: Boolean): Boolean = not(and(a, b))

  def nor(a: Boolean, b: Boolean): Boolean = not(or(a, b))

  def xor(a: Boolean, b: Boolean): Boolean = a ^ b

  def impl(a: Boolean, b: Boolean): Boolean = or(not(a), b)

  def equ(a: Boolean, b: Boolean): Boolean = or(and(a, b), and(not(a), not(b)))

  def table2(fun: (Boolean, Boolean) => Boolean): Map[(Boolean, Boolean), Boolean] = Map(
    (false, false) -> fun(false, false),
    (false, true) -> fun(false, true),
    (true, false) -> fun(true, false),
    (true, true) -> fun(true, true)
  )

  //  P49 (**) Gray code.
  def gray(length: Int) = {
    @tailrec
    def gray(length: Int, acc: List[List[Char]]): List[String] = length match {
      case 1 => acc map (_.mkString)
      case xs => gray(xs - 1, (acc map ('0' :: _)) ::: (acc.reverse map ('1' :: _)))
    }
    require(length > 0)
    gray(length, List(List('0'), List('1')))
  }

  //  P50 (***) Huffman code.
  def huffman(list: List[(String, Int)]): List[(String, String)] = ???

}
