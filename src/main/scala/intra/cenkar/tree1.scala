package intra.cenkar

import scala.math.{max, pow}

package binarytree {

sealed abstract class Tree[+T] {

  //  P56 (**) Symmetric binary trees.
  def isSymmetric: Boolean = ???

  def isMirrorOf[A](other: Tree[A]): Boolean = ???

  //  P57 (**) Binary search trees (dictionaries).
  def addValue[U >: T <% Ordered[U]](x: U): Tree[U] = ???

  //  P61 (*) Count the leaves of a binary tree.
  def leafCount: Int = ???

  //  61A (*) Collect the leaves of a binary tree in a list.
  def leafList: List[T] = ???

  //  P62 (*) Collect the internal nodes of a binary tree in a list.
  def internalList: List[T] = ???

  //  P62B (*) Collect the nodes at a given level in a list.
  def atLevel(level: Int): List[T] = ???

  def inOrder: List[T] = ???

  def preOrder: List[T] = ???

  def height: Int = ???

  def toCompactString: String = ???

  def toDotString: String = ???

}

case class Node[+T](value: T, left: Tree[T], right: Tree[T]) extends Tree[T] {

  override def toString = "T(" + value.toString + " " + left.toString + " " + right.toString + ")"

  //  P67 (**) A string representation of binary trees.
  override def toCompactString = this match {
    case Node(_, End, End) => value.toString
    case _ => s"$value(${left.toCompactString},${right.toCompactString})"
  }

  override def isMirrorOf[A](other: Tree[A]): Boolean = other match {
    case Node(_, l, r) => (left isMirrorOf r) && (right isMirrorOf l)
    case End => false
  }

  //  P56 (**) Symmetric binary trees.
  override def isSymmetric: Boolean = left isMirrorOf right

  //  P57 (**) Binary search trees (dictionaries).
  override def addValue[U >: T <% Ordered[U]](x: U): Tree[U] = {
    if (x == value) this
    else if (x < value) Node(value, left.addValue(x), right)
    else Node(value, left, right.addValue(x))
  }

  override def leafCount: Int = this match {
    case Node(_, End, End) => 1
    case _ => left.leafCount + right.leafCount
  }

  //  61A (*) Collect the leaves of a binary tree in a list.
  override def leafList: List[T] = this match {
    case Node(_, End, End) => List(value)
    case _ => left.leafList ::: right.leafList
  }

  override def internalList: List[T] = this match {
    case Node(_, End, End) => Nil
    case _ => value :: left.internalList ::: right.internalList
  }

  //  P62B (*) Collect the nodes at a given level in a list.
  override def atLevel(level: Int): List[T] = level match {
    case 1 => List(value)
    case x => left.atLevel(x - 1) ::: right.atLevel(x - 1)
  }

  override def inOrder: List[T] = left.inOrder ::: value :: right.inOrder

  override def preOrder: List[T] = value :: left.preOrder ::: right.preOrder

  override def height: Int = 1 + max(left.height, right.height)

  //  P64 (**) Layout a binary tree (1).
  def layoutBinaryTree: PositionedNode[T] = {
    val toMap = (inOrder.zipWithIndex map { x => (x._1, x._2 + 1)}) toMap
    def layoutBinaryTree(currentLevel: Int, node: Tree[T]): PositionedNode[T] = node match {
      case Node(v, End, End) => PositionedNode(v, End, End, toMap(v), currentLevel)
      case Node(v, End, x: Node[T]) => PositionedNode(v, End, layoutBinaryTree(currentLevel + 1, x), toMap(v), currentLevel)
      case Node(v, x: Node[T], End) => PositionedNode(v, layoutBinaryTree(currentLevel + 1, x), End, toMap(v), currentLevel)
      case Node(v, x: Node[T], y: Node[T]) => PositionedNode(v, layoutBinaryTree(currentLevel + 1, x), layoutBinaryTree(currentLevel + 1, y),
        toMap(v), currentLevel)
    }
    layoutBinaryTree(1, this)
  }

  //  P65 (**) Layout a binary tree (2).
  def layoutBinaryTree2: PositionedNode[T] = {
    def calculateRootX: Int = {
      val h = height - 1
      ((1 to leftBranchesFromRoot) map { x => pow(2, h - x).toInt}).reduce(_ + _) + 1
    }
    def leftBranchesFromRoot: Int = {
      def leftBranchesFromRoot(current: Int, node: Tree[T]): Int = node match {
        case Node(_, End, _) => current
        case Node(_, l, _) => leftBranchesFromRoot(current + 1, l)
        case End => current
      }
      leftBranchesFromRoot(0, this)
    }
    val h = height
    def layoutBinaryTree2(x: Int, level: Int, node: Tree[T]): PositionedNode[T] = node match {
      case Node(v, l: Node[T], r: Node[T]) => PositionedNode(v, layoutBinaryTree2(x - pow(2, h - (level + 1)).toInt, level + 1, l),
        layoutBinaryTree2(x + pow(2, h - (level + 1)).toInt, level + 1, r), x, level)
      case Node(v, End, r: Node[T]) => PositionedNode(v, End, layoutBinaryTree2(x + pow(2, h - (level + 1)).toInt, level + 1, r), x, level)
      case Node(v, l: Node[T], End) => PositionedNode(v, layoutBinaryTree2(x - pow(2, h - (level + 1)).toInt, level + 1, l), End, x, level)
      case Node(v, End, End) => PositionedNode(v, End, End, x, level)
    }
    layoutBinaryTree2(calculateRootX, 1, this)
  }

  //  P66 (***) Layout a binary tree (3).
  def layoutBinaryTree3: PositionedNode[T] = ???

  //  P69 (**) Dotstring representation of binary trees.
  override def toDotString: String = value + left.toDotString + right.toDotString

}

case class PositionedNode[+T](val value: T, val left: Tree[T], val right: Tree[T], x: Int, y: Int) extends Tree[T] {
  override def toString = "T[" + x.toString + "," + y.toString + "](" + value.toString + " " + left.toString + " " + right.toString + ")"
}

case object End extends Tree[Nothing] {

  override def toString = "."

  override def toCompactString = ""

  override def isMirrorOf[A](other: Tree[A]): Boolean = other == this

  override def isSymmetric: Boolean = true

  override def addValue[U >: Nothing <% Ordered[U]](x: U): Tree[U] = Node(x)

  override def leafCount: Int = 0

  override def leafList: List[Nothing] = Nil

  override def internalList: List[Nothing] = Nil

  override def atLevel(level: Int): List[Nothing] = Nil

  override def inOrder: List[Nothing] = Nil

  override def preOrder: List[Nothing] = Nil

  override def height: Int = 0

  override def toDotString: String = "."

}

object Node {
  def apply[T](value: T): Node[T] = Node(value, End, End)

  def apply[T](value: T, left: Node[T], right: Node[T]): Node[T] = new Node(value, left, right)
}

object Tree {

  //  P55 (**) Construct completely balanced binary trees.
  def cBalanced[A](nodes: Int, value: A): List[Tree[A]] = ???

  def fromList[A <% Ordered[A]](values: List[A]): Tree[A] = values.foldLeft[Tree[A]](End)(_.addValue(_))

  //  P63 (**) Construct a complete binary tree.
  def completeBinaryTree[A](nodes: Int, value: A): Tree[A] = {
    def completeBinaryTree(currentLevel: Int, remainingNodes: Int): IndexedSeq[Tree[A]] = {
      val nodesOnCurrentLevel = Math.pow(2, currentLevel - 1).toInt
      if (nodesOnCurrentLevel >= remainingNodes) ((1 to nodesOnCurrentLevel) map { x => if (x > remainingNodes) End else Node(value)})
      else {
        val balanced = completeBinaryTree(currentLevel + 1, remainingNodes - nodesOnCurrentLevel)
        ((0 to nodesOnCurrentLevel - 1) map { x => Node(value, balanced(2 * x), balanced(2 * x + 1))})
      }
    }

    require(nodes > 0)
    val balanced = completeBinaryTree(2, nodes - 1)
    Node(value, balanced.head, balanced.tail.head)
  }

  def completeBinaryTree2[A](nodes: Int, value: A): Tree[A] = {
    def completeBinaryTree2(currentNode: Int): Tree[A] = {
      if (currentNode <= nodes) Node(value, completeBinaryTree2(currentNode * 2), completeBinaryTree2(currentNode * 2 + 1))
      else End
    }
    completeBinaryTree2(1)
  }

  //  P67 (**) A string representation of binary trees.
  def fromString(string: String): Tree[Char] = {
    def findNonNestedCharacter(string: String, char: Char): Int = {
      var (i, opened) = (0, 0)
      while (string(i) != char || opened != 0) {
        if (string(i) == '(')
          opened = opened + 1
        if (string(i) == ')')
          opened = opened - 1
        i = i + 1
      }
      i
    }
    def fromString(list: List[Char]): Tree[Char] = list match {
      case (c: Char) :: Nil => Node(c)
      case Nil => End
      case value :: '(' :: nested => {
        val slice: List[Char] = nested.slice(0, findNonNestedCharacter(nested mkString, ')'))
        val separator: Int = findNonNestedCharacter(slice mkString, ',')
        Node(value, fromString(slice.slice(0, separator)), fromString(slice.slice(separator + 1, slice.length)))
      }
    }
    fromString(string.toList)
  }

  //  P68 (**) Preorder and inorder sequences of binary trees.
  def preInTree[A](preOrderTraversal: List[A], inOrderTraversal: List[A]): Tree[A] = preOrderTraversal match {
    case xs :: Nil => Node(xs)
    case Nil => End
    case xs :: tail => {
      val (leftInOrder, rightInOrder): (List[A], List[A]) = inOrderTraversal.span(_ != xs)
      val (leftPreOrder, rightPreOrder): (List[A], List[A]) = tail.splitAt(leftInOrder.length)
      Node(xs, preInTree(leftPreOrder, leftInOrder), preInTree(rightPreOrder, rightInOrder.tail))
    }
  }

  //  P69 (**) Dotstring representation of binary trees.
  def fromDotString(string: String): Tree[Char] = {
    def fromDotString(currentPosition: Int): (Tree[Char], Int) = string(currentPosition) match {
      case '.' => (End, currentPosition + 1)
      case letter => {
        val (leftTree, leftPos) = fromDotString(currentPosition + 1)
        val (rightTree, rightPos) = fromDotString(leftPos)
        (Node(letter, leftTree, rightTree), rightPos)
      }
    }
    fromDotString(0)._1
  }
}

}
