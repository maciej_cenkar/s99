package intra.cenkar

package multiwaytree {

case class MTree[+T](value: T, children: List[MTree[T]]) {

  override def toString = value.toString + (children map (_.toString)).mkString + "^"

  //  P70C (*) Count the nodes of a multiway tree.
  def nodeCount: Int = 1 + children.foldLeft(0)((acc, tree) => acc + tree.nodeCount)

  //  P71 (*) Determine the internal path length of a tree.
  def internalPathLength: Int = internalPathLength(0)

  private def internalPathLength(level: Int): Int = level + (children map (_.internalPathLength(level + 1))).sum

  //  P72 (*) Construct the postorder sequence of the tree nodes.
  def postorder: List[T] = (children flatMap (_.postorder)) ::: List(value)

  //  P73 (**) Lisp-like tree representation.
  def lispyTree: String = this match {
    case MTree(x, Nil) => x.toString
    case MTree(x, children) => "(" + x + " " + (children map (_.lispyTree)).mkString(" ") + ")"
  }
}

object MTree {

  def apply[T](value: T): MTree[T] = new MTree(value, Nil)

  //  P70 (**) Tree construction from a node string.
  def fromString(string: String): MTree[Char] = {
    def fromString(currentPosition: Int): (List[MTree[Char]], Int) = string(currentPosition) match {
      case '^' => (Nil, currentPosition + 1)
      case letter => {
        val (sub, pos): (List[MTree[Char]], Int) = fromString(currentPosition + 1)
        val (right, maxPos): (List[MTree[Char]], Int) = fromString(pos)
        (MTree(letter, sub) :: right, maxPos)
      }
    }
    MTree(string(0), fromString(1)._1)
  }

  //  P73 (**) Lisp-like tree representation.
  def fromLispyTree(string: String): MTree[Char] = ???
}

}