package intra.cenkar

package arithmetic {

class S99Int(val start: Int) {

  def isPrime: Boolean = Arithmetic.isPrime(start)

  def isCoprimeTo(other: Int) = Arithmetic.isCoprime(start, other)

  def totient = Arithmetic.totient(start)

  def primeFactors = Arithmetic.primeFactors(start)

  def primeFactorMultiplicity = Arithmetic.primeFactorMultiplicity(start)

  def totient2 = Arithmetic.totient2(start)

}

class S99Logic(val start: Boolean) {

  //  P47 (*) Truth tables for logical expressions (2).
  def and(other: Boolean): Boolean = Logic.and(start, other)

  def nand(other: Boolean): Boolean = Logic.nand(start, other)

  def or(other: Boolean): Boolean = Logic.or(start, other)

  def nor(other: Boolean): Boolean = Logic.nor(start, other)

  def xor(other: Boolean): Boolean = Logic.xor(start, other)

  def impl(other: Boolean): Boolean = Logic.impl(start, other)

  def equ(other: Boolean): Boolean = Logic.equ(start, other)

}

object S99Int {
  implicit def int2S99Int(i: Int): S99Int = new S99Int(i)
}

object S99Logic {

  implicit def bool2S99Logic(a: Boolean): S99Logic = new S99Logic(a)

  def neg(a: Boolean): Boolean = Logic.not(a)

}

}