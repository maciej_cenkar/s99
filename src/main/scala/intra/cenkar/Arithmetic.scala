package intra.cenkar

import scala.annotation.tailrec
import scala.math.{ceil, pow, sqrt}

object Arithmetic {

  //  P31 (**) Determine whether a given integer number is prime.
  def isPrime(candidate: Int): Boolean = {
    require(candidate > 0)
    if (candidate == 1) false else (2 to ceil(sqrt(candidate)).toInt).find(x => x != candidate && candidate % x == 0).isEmpty
  }

  //  P32 (**) Determine the greatest common divisor of two positive integer numbers.
  @tailrec
  def gcd(x: Int, y: Int): Int = if (y == 0) x else gcd(y, x % y)

  //  P33 (*) Determine whether two positive integer numbers are coprime.
  def isCoprime(x: Int, y: Int): Boolean = gcd(x, y) == 1

  //  P34 (**) Calculate Euler's totient function phi(m).
  def totient(x: Int): Int = ((1 to x) filter (isCoprime(x, _))).size

  //  P35 (**) Determine the prime factors of a given positive integer.
  def primeFactors(x: Int): List[Int] = x match {
    case 1 => Nil
    case xs => {
      val option: Int = ((2 to x) find (x % _ == 0)).get
      option :: primeFactors(x / option)
    }
  }

  //  P36 (**) Determine the prime factors of a given positive integer (2).
  def primeFactorMultiplicity(x: Int): List[(Int, Int)] = Lists.encode(primeFactors(x)) map (_.swap)

  //  P37 (**) Calculate Euler's totient function phi(m) (improved).
  def totient2(x: Int): Int = primeFactorMultiplicity(x) map { case (p, m) => (p - 1) * pow(p, m - 1).toInt} reduce (_ * _)

  //  P39 (*) A list of prime numbers.
  def listPrimesinRange(range: Range): List[Int] = (range filter (isPrime(_))).toList

  //  P40 (**) Goldbach's conjecture.
  def goldbach(x: Int): (Int, Int) = twoSum(listPrimesinRange(2 to x).toIndexedSeq, x)

  def twoSum(seq: IndexedSeq[Int], expected: Int): (Int, Int) = {
    @tailrec
    def twoSum(start: Int, end: Int): (Int, Int) = {
      val sum: Int = seq(start) + seq(end)
      if (end < start) throw new IllegalArgumentException
      else if (sum == expected) (seq(start), seq(end))
      else if (sum > expected) twoSum(start, end - 1)
      else twoSum(start + 1, end)
    }
    twoSum(0, seq.length - 1)
  }

  //  P41 (**) A list of Goldbach compositions.
  def printGoldbachList(range: Range): Map[Int, (Int, Int)] = Map(range filter (_ % 2 == 0) map (x => (x, goldbach(x))): _*)

}
