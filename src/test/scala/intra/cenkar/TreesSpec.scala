package intra.cenkar

import intra.cenkar.binarytree._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class TreesSpec extends FunSpec with Matchers {

  describe("P56") {
    it("should recognize mirror images") {
      (End isMirrorOf End) should be(true)
      (End isMirrorOf Node('X)) should be(false)
      (Node('X) isMirrorOf End) should be(false)
      (Node('X) isMirrorOf Node('X)) should be(true)
      (Node('X, Node('Y), End) isMirrorOf Node('A, End, Node('B))) should be(true)
      (Node('A, End, Node('B)) isMirrorOf Node('X, Node('Y), End)) should be(true)
      (Node('A, End, Node('B)) isMirrorOf Node('X, End, End)) should be(false)
      (Node('A, Node('B), Node('C)) isMirrorOf Node('X, Node('Y), Node('Z))) should be(true)
      (Node('A, Node('B, End, Node('C)), End) isMirrorOf Node('X, End, Node('Y, Node('Z), End))) should be(true)
      (Node('A, Node('B), Node('C)) isMirrorOf Node('X, Node('Y), Node('Z))) should be(true)
      (Node('A, Node('B), Node('C)) isMirrorOf Node('X, Node('Y), End)) should be(false)
    }

    it("should recognize symetric trees") {
      Node('a', Node('b'), Node('c')).isSymmetric should be(true)
      End.isSymmetric should be(true)
      Node('X, Node('Y, Node('Z), End), Node('Y, End, Node('Z))).isSymmetric should be(true)
      Node('X, Node('Y, Node('Z), End), Node('Y, Node('V), Node('Z))).isSymmetric should be(false)
      Tree.fromList(List(3, 2, 5, 7, 4)).isSymmetric should be(false)
    }
  }

  describe("P57") {
    it("should add values to BST") {
      End.addValue(2) should be(Node(2))
      End.addValue(2).addValue(3) should be(Node(2, End, Node(3)))
      End.addValue(2).addValue(3).addValue(0) should be(Node(2, Node(0), Node(3)))
      Tree.fromList(List(3, 2, 5, 7, 1)) should be(Node(3, Node(2, Node(1), End), Node(5, End, Node(7))))
    }
  }

  describe("P61") {
    it("should count leaves") {
      Node('x).leafCount should be(1)
      Node('x', Node('x'), End).leafCount should be(1)
      Node('x', End, Node('x')).leafCount should be(1)
      Node('x', Node('x'), Node('x')).leafCount should be(2)
      Node('x', Node('x', Node('x'), End), Node('x')).leafCount should be(2)
      Node('x', Node('x', Node('x'), Node('x')), Node('x')).leafCount should be(3)
      Node('x', Node('x', Node('x'), Node('x')), Node('x', Node('x'), End)).leafCount should be(3)
      Node('x', Node('x', Node('x'), Node('x')), Node('x', Node('x'), Node('x'))).leafCount should be(4)
      Node('x', Node('x', Node('x', Node('x'), End), Node('x')), Node('x', Node('x'), Node('x'))).leafCount should be(4)
      Node('x', Node('x', Node('x', Node('x'), Node('x')), Node('x')), Node('x', Node('x'), Node('x'))).leafCount should be(5)
    }
  }

  describe("P61A") {
    it("should collect leaves into list") {
      Node('x', Node('y'), End).leafList should be(List('y'))
      Node('a', Node('b'), Node('c', Node('d'), Node('e'))).leafList should be(List('b', 'd', 'e'))
      Node('a', Node('b', Node('c', Node('d'), Node('e')), Node('f')), Node('g', Node('h'), Node('i'))).leafList should be(
        List('d', 'e', 'f', 'h', 'i')
      )
    }
  }

  describe("P62") {
    it("should collect internal nodes") {
      Node('a', Node('b'), Node('c', Node('d'), Node('e'))).internalList should be(List('a', 'c'))
      Node('x', Node('y'), End).internalList should be(List('x'))
      Node('a', Node('b'), Node('c', Node('d'), Node('e'))).internalList should be(List('a', 'c'))
      Node('a', Node('b', Node('c', Node('d'), Node('e')), Node('f')), Node('g', Node('h'), Node('i'))).internalList should be(
        List('a', 'b', 'c', 'g'))
    }
  }

  describe("P62") {
    it("should give nodes at level") {
      Node('a', Node('b'), Node('c', Node('d'), Node('e'))).atLevel(1) should be(List('a'))
      Node('a', Node('b'), Node('c', Node('d'), Node('e'))).atLevel(2) should be(List('b', 'c'))
      Node('a', Node('b'), Node('c', Node('d'), Node('e'))).atLevel(3) should be(List('d', 'e'))
      Node('a', Node('b'), Node('c', Node('d'), Node('e', Node('f'), Node('g')))).atLevel(4) should be(List('f', 'g'))
    }
  }

  describe("P63") {
    it("should create complete binary tree") {
      Tree.completeBinaryTree(1, 'A) should be(Node('A))
      Tree.completeBinaryTree(2, 'A) should be(Node('A, Node('A), End))
      Tree.completeBinaryTree(3, 'A) should be(Node('A, Node('A), Node('A)))
      Tree.completeBinaryTree(4, 'A) should be(Node('A, Node('A, Node('A), End), Node('A)))
      Tree.completeBinaryTree(5, 'A) should be(Node('A, Node('A, Node('A), Node('A)), Node('A)))
      Tree.completeBinaryTree(6, 'A) should be(Node('A, Node('A, Node('A), Node('A)), Node('A, Node('A), End)))
      Tree.completeBinaryTree(7, 'A) should be(Node('A, Node('A, Node('A), Node('A)), Node('A, Node('A), Node('A))))
      Tree.completeBinaryTree(8, 'A) should be(Node('A, Node('A, Node('A, Node('A), End), Node('A)), Node('A, Node('A), Node('A))))
      Tree.completeBinaryTree(9, 'A) should be(Node('A, Node('A, Node('A, Node('A), Node('A)), Node('A)), Node('A, Node('A), Node('A))))
    }

    it("should create complete binary tree - alternative") {
      Tree.completeBinaryTree2(1, 'A) should be(Node('A))
      Tree.completeBinaryTree2(2, 'A) should be(Node('A, Node('A), End))
      Tree.completeBinaryTree2(3, 'A) should be(Node('A, Node('A), Node('A)))
      Tree.completeBinaryTree2(4, 'A) should be(Node('A, Node('A, Node('A), End), Node('A)))
      Tree.completeBinaryTree2(5, 'A) should be(Node('A, Node('A, Node('A), Node('A)), Node('A)))
      Tree.completeBinaryTree2(6, 'A) should be(Node('A, Node('A, Node('A), Node('A)), Node('A, Node('A), End)))
      Tree.completeBinaryTree2(7, 'A) should be(Node('A, Node('A, Node('A), Node('A)), Node('A, Node('A), Node('A))))
      Tree.completeBinaryTree2(8, 'A) should be(Node('A, Node('A, Node('A, Node('A), End), Node('A)), Node('A, Node('A), Node('A))))
      Tree.completeBinaryTree2(9, 'A) should be(Node('A, Node('A, Node('A, Node('A), Node('A)), Node('A)), Node('A, Node('A), Node('A))))
    }

  }

  describe("P64") {
    it("should layout tree") {
      (Node('a', Node('b', End, Node('c')), Node('d')).layoutBinaryTree).toString should be(
        "T[3,1](a T[1,2](b . T[2,3](c . .)) T[4,2](d . .))")
      Tree.fromList(List('n', 'k', 'm', 'c', 'a', 'h', 'g', 'e', 'u', 'p', 's', 'q'))
        .asInstanceOf[Node[Char]].layoutBinaryTree.toString should be(
        "T[8,1](n T[6,2](k T[2,3](c T[1,4](a . .) T[5,4](h T[4,5](g T[3,6](e . .) .) .)) T[7,3](m . .)) T[12,2](u T[9,3](p . T[11,4](s T[10,5](q . .) .)) .))")
    }
  }

  describe("P65") {
    it("should layout tree") {
      Node('a, Node('b, End, Node('c)), Node('d)).layoutBinaryTree2.toString should be(
        "T[3,1]('a T[1,2]('b . T[2,3]('c . .)) T[5,2]('d . .))")
      Tree.fromList(List('n', 'k', 'm', 'c', 'a', 'e', 'd', 'g', 'u', 'p', 'q')).asInstanceOf[Node[Char]].layoutBinaryTree2.toString should be(
        "T[15,1](n T[7,2](k T[3,3](c T[1,4](a . .) T[5,4](e T[4,5](d . .) T[6,5](g . .))) T[11,3](m . .)) T[23,2](u T[19,3](p . T[21,4](q . .)) .))"
      )
    }
  }

  describe("P67") {
    it("should create correct string from tree") {
      Node('a', Node('b', Node('d'), Node('e')), Node('c', End, Node('f', Node('g'), End))).toCompactString should be("a(b(d,e),c(,f(g,)))")
    }

    it("should parse string representation") {
      Tree.fromString("") should be(End)
      Tree.fromString("a") should be(Node('a'))
      Tree.fromString("a(b,c)") should be(Node('a', Node('b'), Node('c')))
      Tree.fromString("a(,c)") should be(Node('a', End, Node('c')))
      Tree.fromString("a(b,)") should be(Node('a', Node('b'), End))
      Tree.fromString("a(b(c,d),e(f,g))") should be(
        Node('a', Node('b', Node('c'), Node('d')), Node('e', Node('f'), Node('g'))))
      Tree.fromString("a(b(d,e),c(,f(g,)))") should be(Node('a', Node('b', Node('d'), Node('e')), Node('c', End, Node('f', Node('g'), End))))
    }
  }

  describe("P68") {
    it("should create pre and in order traversals") {
      val string: Tree[Char] = Tree.fromString("a(b(d,e),c(,f(g,)))")
      Tree.fromString("a(b(d,e),c(,f(g,)))").inOrder should be(List('d', 'b', 'e', 'a', 'c', 'g', 'f'))
      string.preOrder should be(List('a', 'b', 'd', 'e', 'c', 'f', 'g'))
    }

    it("should create tree from in- and pre- order traversal") {
      Tree.preInTree(List('f', 'g'), List('g', 'f')) should be(Node('f', Node('g'), End))
      Tree.preInTree(List('c', 'f', 'g'), List('c', 'g', 'f')) should be(Node('c', End, Node('f', Node('g'), End)))
      Tree.preInTree(List('a', 'b', 'd', 'e', 'c', 'f', 'g'), List('d', 'b', 'e', 'a', 'c', 'g', 'f')).toCompactString should be(
        "a(b(d,e),c(,f(g,)))")
    }
  }

  describe("P69") {
    it("should create dot string") {
      Tree.fromString("a(b(d,e),c(,f(g,)))").toDotString should be("abd..e..c.fg...")
    }

    it("should parse dot string") {
      Tree.fromDotString("ab..d..").toCompactString should be("a(b,d)")
      Tree.fromDotString("a.d..").toCompactString should be("a(,d)")
      Tree.fromDotString("ab...").toCompactString should be("a(b,)")
      Tree.fromDotString("c.fg...").toCompactString should be("c(,f(g,))")
      Tree.fromDotString("abd..e..c.fg...").toCompactString should be("a(b(d,e),c(,f(g,)))")
    }
  }

}
