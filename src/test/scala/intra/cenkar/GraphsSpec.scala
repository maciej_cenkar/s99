package intra.cenkar

import intra.cenkar.graph.{Digraph, Graph}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class GraphsSpec extends FunSpec with Matchers {

  describe("P80") {
    it("should parse string for undirected graph and create term-form") {
      val (nodes, edges): (List[Char], List[(Char, Char, Unit)]) = Graph.fromString("b-c, f-c, g-h, d, f-b, k-f, h-g").toTermForm
      nodes.toSet should be(Set('d', 'k', 'h', 'c', 'f', 'g', 'b'))
      edges.toSet should be(Set(('h', 'g', ()), ('k', 'f', ()), ('f', 'b', ()), ('g', 'h', ()), ('f', 'c', ()), ('b', 'c', ())))
    }

    it("should parse string for directed graph and create adjacent-form") {
      Digraph.fromStringLabel("p>q/9, m>q/7, k, p>m/5").toAdjacentForm.toSet should be(
        Set(('m', List(('q', 7))), ('p', List(('m', 5), ('q', 9))), ('k', List()), ('q', List())))
    }
  }

}
