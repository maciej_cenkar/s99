package intra.cenkar

import intra.cenkar.Lists._
import org.junit.runner.RunWith
import org.scalatest.{FunSpec, Matchers}
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ListsSpec extends FunSpec with Matchers {

  describe("P01") {
    it("should return last element of list") {
      last(List(1, 1, 2, 3, 5, 8)) should be(8)
    }

    it("should throw exception on empty list") {
      intercept[IllegalArgumentException] {
        last(List.empty)
      }
    }
  }

  describe("P02") {
    it("should return penultimate element") {
      penultimate(List(1, 1, 2, 3, 5, 8)) should be(5)
      penultimate(List(1, 1, 2, 3, 5)) should be(3)
      penultimate(List(1, 1, 2, 3)) should be(2)
      penultimate(List(1, 1, 2)) should be(1)
      penultimate(List(1, 1)) should be(1)
    }

    it("should throw exception on too short lists") {
      intercept[IllegalArgumentException] {
        penultimate(List.empty)
      }

      intercept[IllegalArgumentException] {
        penultimate(List(1))
      }
    }
  }

  describe("P03") {
    it("should return nth element") {
      nth(0, List(1, 1, 2, 3, 5, 8)) should be(1)
      nth(1, List(1, 1, 2, 3, 5, 8)) should be(1)
      nth(2, List(1, 1, 2, 3, 5, 8)) should be(2)
      nth(3, List(1, 1, 2, 3, 5, 8)) should be(3)
      nth(4, List(1, 1, 2, 3, 5, 8)) should be(5)
      nth(5, List(1, 1, 2, 3, 5, 8)) should be(8)
      nth(2, List(1, 1, 2, 3, 5)) should be(2)
      nth(2, List(1, 1, 2, 3)) should be(2)
      nth(2, List(1, 1, 2)) should be(2)
      nth(1, List(1, 1)) should be(1)
      nth(0, List(1)) should be(1)
    }

    it("should throw exception on n bigger than list") {
      intercept[IllegalArgumentException] {
        nth(2, List(1))
      }
    }

    it("should throw exception on empty list") {
      intercept[IllegalArgumentException] {
        nth(42, Nil)
      }
    }
  }

  describe("P04") {
    it("should return list length") {
      Lists.length(List(1, 1, 2, 3, 5, 8)) should be(6)
      Lists.length(List(1, 1, 2, 3, 5)) should be(5)
      Lists.length(List(1, 1, 2, 3)) should be(4)
      Lists.length(List(1, 1, 2)) should be(3)
      Lists.length(List(1, 1)) should be(2)
      Lists.length(List(1)) should be(1)
      Lists.length(Nil) should be(0)

      Lists.lengthtr(List(1, 1, 2, 3, 5, 8)) should be(6)
      Lists.lengthtr(List(1, 1, 2, 3, 5)) should be(5)
      Lists.lengthtr(List(1, 1, 2, 3)) should be(4)
      Lists.lengthtr(List(1, 1, 2)) should be(3)
      Lists.lengthtr(List(1, 1)) should be(2)
      Lists.lengthtr(List(1)) should be(1)
      Lists.lengthtr(Nil) should be(0)
    }
  }

  describe("P05") {
    it("should reverse given list") {
      reverse(List(1, 1, 2, 3, 5, 8)) should be(List(8, 5, 3, 2, 1, 1))
      reverse(List(1, 1, 2, 3, 5)) should be(List(5, 3, 2, 1, 1))
      reverse(List(1, 1, 2, 3)) should be(List(3, 2, 1, 1))
      reverse(List(1, 1, 2)) should be(List(2, 1, 1))
      reverse(List(1, 1)) should be(List(1, 1))
      reverse(List(1)) should be(List(1))
      reverse(Nil) should be(Nil)
    }
  }

  describe("P06") {
    it("should recognize palindromes") {
      isPalindrome(List(1, 2, 3, 2, 1)) should be(true)
      isPalindrome(List(2, 3, 2)) should be(true)
      isPalindrome(List(3)) should be(true)
    }

    it("should recognize non-palindromes") {
      isPalindrome(List(1, 3, 3, 2, 1)) should be(false)
      isPalindrome(List(2, 3, 1)) should be(false)
      isPalindrome(List(1, 3, 3, 4)) should be(false)
      isPalindrome(List(1, 3)) should be(false)
    }
  }

  describe("P07") {
    it("should flatten given list") {
      flatten(List(List(1, 1), 2, List(3, List(5, 8)))) should be(List(1, 1, 2, 3, 5, 8))
      flatten(List(List(1, 1), List(3, List(5, 8)))) should be(List(1, 1, 3, 5, 8))
      flatten(List(List(1), 2, List(3, List(5, 8)))) should be(List(1, 2, 3, 5, 8))
      flatten(List(List(1, 1), 2, List(3, List(5)))) should be(List(1, 1, 2, 3, 5))
      flatten(List(List(1, 1), List(3, List(5, 8)))) should be(List(1, 1, 3, 5, 8))
      flatten(List(2, List(3, List(5, 8)))) should be(List(2, 3, 5, 8))
      flatten(List(List(1, 1), 2, List(3))) should be(List(1, 1, 2, 3))
      flatten(List(2, List(3))) should be(List(2, 3))
      flatten(List(2, 3)) should be(List(2, 3))
    }
  }

  describe("P08") {
    it("should compress given list") {
      compress(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) should be(List('a, 'b, 'c, 'a, 'd, 'e))
      compress(List('a, 'a, 'b, 'a)) should be(List('a, 'b, 'a))
      compress(List('a)) should be(List('a))
      compress(Nil) should be(Nil)
    }
  }

  describe("P09") {
    it("should pack elements into lists") {
      pack(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) should be(
        List(List('a, 'a, 'a, 'a), List('b), List('c, 'c), List('a, 'a), List('d), List('e, 'e, 'e, 'e)))
      pack(Nil) should be(Nil)
    }
  }

  describe("P10") {
    it("should run-length encode list") {
      encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) should be(
        List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e)))
      encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd)) should be(List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd)))
      encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a)) should be(List((4, 'a), (1, 'b), (2, 'c), (2, 'a)))
      encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c)) should be(List((4, 'a), (1, 'b), (2, 'c)))
      encode(List('a, 'a, 'a, 'a, 'b)) should be(List((4, 'a), (1, 'b)))
      encode(List('a, 'a, 'a, 'a)) should be(List((4, 'a)))
      encode(Nil) should be(Nil)
    }
  }

  describe("P11") {
    it("should run-length encode list without duplicates") {
      encodeModified(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) should be(
        List((4, 'a), 'b, (2, 'c), (2, 'a), 'd, (4, 'e)))
      encodeModified(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd)) should be(List((4, 'a), 'b, (2, 'c), (2, 'a), 'd))
      encodeModified(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a)) should be(List((4, 'a), 'b, (2, 'c), (2, 'a)))
      encodeModified(List('a, 'a, 'a, 'a, 'b, 'c, 'c)) should be(List((4, 'a), 'b, (2, 'c)))
      encodeModified(List('a, 'a, 'a, 'a, 'b)) should be(List((4, 'a), 'b))
      encodeModified(List('a, 'a, 'a, 'a)) should be(List((4, 'a)))
      encodeModified(List('a)) should be(List('a))
    }
  }

  describe("P12") {
    it("should decode run-length list") {
      decode(List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e))) should be(
        List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
      decode(List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd))) should be(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd))
      decode(List((4, 'a), (1, 'b), (2, 'c), (2, 'a))) should be(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a))
      decode(List((4, 'a), (1, 'b), (2, 'c))) should be(List('a, 'a, 'a, 'a, 'b, 'c, 'c))
      decode(List((4, 'a), (1, 'b))) should be(List('a, 'a, 'a, 'a, 'b))
      decode(List((4, 'a))) should be(List('a, 'a, 'a, 'a))
      decode(List((1, 'a))) should be(List('a))
      decode(Nil) should be(Nil)
    }
  }

  describe("P13") {
    it("should encode list directly") {
      encodeDirect(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) should be(
        List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e)))
      encodeDirect(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd)) should be(
        List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd)))
      encodeDirect(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a)) should be(List((4, 'a), (1, 'b), (2, 'c), (2, 'a)))
      encodeDirect(List('a, 'a, 'a, 'a, 'b, 'c, 'c)) should be(List((4, 'a), (1, 'b), (2, 'c)))
      encodeDirect(List('a, 'a, 'a, 'a, 'b)) should be(List((4, 'a), (1, 'b)))
      encodeDirect(List('a, 'a, 'a, 'a)) should be(List((4, 'a)))
      encodeDirect(List('a)) should be(List((1, 'a)))
      encodeDirect(Nil) should be(Nil)
    }
  }

  describe("P14") {
    it("should duplicate elements of list") {
      duplicate(List('a, 'b, 'c, 'c, 'd)) should be(List('a, 'a, 'b, 'b, 'c, 'c, 'c, 'c, 'd, 'd))
      duplicate(List('a, 'b, 'c, 'c)) should be(List('a, 'a, 'b, 'b, 'c, 'c, 'c, 'c))
      duplicate(List('a, 'b, 'c)) should be(List('a, 'a, 'b, 'b, 'c, 'c))
      duplicate(List('a, 'b)) should be(List('a, 'a, 'b, 'b))
      duplicate(Nil) should be(Nil)
    }
  }

  describe("P15") {
    it("should duplicate n times") {
      duplicateN(3, List('a, 'b, 'c, 'c, 'd)) should be(List('a, 'a, 'a, 'b, 'b, 'b, 'c, 'c, 'c, 'c, 'c, 'c, 'd, 'd, 'd))
      duplicateN(2, List('a, 'b, 'c, 'c, 'd)) should be(List('a, 'a, 'b, 'b, 'c, 'c, 'c, 'c, 'd, 'd))
      duplicateN(1, List('a, 'b, 'c, 'c, 'd)) should be(List('a, 'b, 'c, 'c, 'd))
      duplicateN(3, List('a, 'b, 'c)) should be(List('a, 'a, 'a, 'b, 'b, 'b, 'c, 'c, 'c))
      duplicateN(3, List('a, 'b)) should be(List('a, 'a, 'a, 'b, 'b, 'b))
      duplicateN(3, List('a)) should be(List('a, 'a, 'a))
      duplicateN(3, Nil) should be(Nil)
    }
  }

  describe("P16") {
    it("should drop every nth element") {
      drop(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(List('a, 'b, 'd, 'e, 'g, 'h, 'j, 'k))
      drop(2, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(List('a, 'c, 'e, 'g, 'i, 'k))
      drop(1, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(Nil)
      drop(2, Nil) should be(Nil)
    }
  }

  describe("P17") {
    it("should split list in two parts") {
      split(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(
        (List('a, 'b, 'c), List('d, 'e, 'f, 'g, 'h, 'i, 'j, 'k)))
      split(2, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(
        (List('a, 'b), List('c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)))
      split(1, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(
        (List('a), List('b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)))
      split(0, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(
        (Nil, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)))
      split(3, List('a, 'b, 'c)) should be(List('a, 'b, 'c), Nil)
      split(0, List('a, 'b, 'c)) should be(Nil, List('a, 'b, 'c))
    }

    it("should throw exception on n bigger than length") {
      intercept[IllegalArgumentException] {
        split(3, List('a, 'b))
      }
    }
  }

  describe("P18") {
    it("should slice given list") {
      slice(3, 7, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(List('d, 'e, 'f, 'g))
      slice(3, 9, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(List('d, 'e, 'f, 'g, 'h, 'i))
      slice(3, 11, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(List('d, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
      slice(1, 7, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(List('b, 'c, 'd, 'e, 'f, 'g))
      slice(0, 7, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(List('a, 'b, 'c, 'd, 'e, 'f, 'g))
      slice(0, 1, List('a)) should be(List('a))
      slice(0, 0, Nil) should be(Nil)
    }

    it("should throw exception on out of bounds") {
      intercept[IllegalArgumentException] {
        slice(-1, 2, List('a, 'b, 'c))
      }

      intercept[IllegalArgumentException] {
        slice(0, 4, List('a, 'b, 'c))
      }

      intercept[IllegalArgumentException] {
        slice(3, 1, List('a, 'b, 'c, 'd, 'e))
      }
    }
  }

  describe("P19") {
    it("should rotate list") {
      rotate(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(
        List('d, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'a, 'b, 'c))
      rotate(-2, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(
        List('j, 'k, 'a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i))
      rotate(5, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) should be(
        List('f, 'g, 'h, 'i, 'j, 'k, 'a, 'b, 'c, 'd, 'e))
      rotate(4, List('a, 'b, 'c, 'd)) should be(List('a, 'b, 'c, 'd))
      rotate(0, List('a, 'b, 'c, 'd)) should be(List('a, 'b, 'c, 'd))
    }

    it("should throw exception on illegal rotation param") {
      intercept[IllegalArgumentException] {
        rotate(3, List('a, 'b))
      }
    }
  }

  describe("P20") {
    it("should remove given element") {
      removeAt(0, List('a, 'b, 'c, 'd)) should be((List('b, 'c, 'd), 'a))
      removeAt(1, List('a, 'b, 'c, 'd)) should be((List('a, 'c, 'd), 'b))
      removeAt(2, List('a, 'b, 'c, 'd)) should be((List('a, 'b, 'd), 'c))
      removeAt(3, List('a, 'b, 'c, 'd)) should be((List('a, 'b, 'c), 'd))
    }

    it("should throw exception on illegal index") {
      intercept[IllegalArgumentException] {
        removeAt(-1, List('a, 'b))
      }
      intercept[IllegalArgumentException] {
        removeAt(2, List('a, 'b))
      }
    }
  }

  describe("P21") {
    it("should insert given element") {
      insertAt('new, 0, List('a, 'b, 'c, 'd)) should be(List('new, 'a, 'b, 'c, 'd))
      insertAt('new, 1, List('a, 'b, 'c, 'd)) should be(List('a, 'new, 'b, 'c, 'd))
      insertAt('new, 2, List('a, 'b, 'c, 'd)) should be(List('a, 'b, 'new, 'c, 'd))
      insertAt('new, 3, List('a, 'b, 'c, 'd)) should be(List('a, 'b, 'c, 'new, 'd))
      insertAt('new, 4, List('a, 'b, 'c, 'd)) should be(List('a, 'b, 'c, 'd, 'new))
    }

    it("should throw exception on invalid index") {
      intercept[IllegalArgumentException] {
        insertAt('a, -1, List('a, 'b))
      }
      intercept[IllegalArgumentException] {
        insertAt('a, 3, List('a, 'b))
      }
    }
  }

  describe("P22") {
    it("should create all integers in given range") {
      range(4, 9) should be(List(4, 5, 6, 7, 8, 9))
      range(4, 7) should be(List(4, 5, 6, 7))
      range(4, 5) should be(List(4, 5))
      range(4, 4) should be(List(4))
      range(-2, 0) should be(List(-2, -1, 0))
    }

    it("should throw exception on illegal range") {
      intercept[IllegalArgumentException] {
        range(5, 3)
      }
    }
  }

  describe("P23") {
    it("should extract random numbers") {
      val fullList: List[Symbol] = List('a, 'b, 'c, 'd, 'f, 'g, 'h)
      val fullSet: Set[Symbol] = fullList.toSet
      val result: Set[Symbol] = randomSelect(3, List('a, 'b, 'c, 'd, 'f, 'g, 'h)).toSet
      (fullSet intersect result).size should be(result.size)
      result.size should be(3)
    }

    it("should extract nothing") {
      randomSelect(0, List('a, 'b)) should be(Nil)
    }

    it("should extract full list") {
      randomSelect(3, List('a, 'b, 'c)).toSet should be(Set('a, 'b, 'c))
    }

    it("should throw exception on illegal argument") {
      intercept[IllegalArgumentException] {
        randomSelect(-1, List('a, 'b))
      }
      intercept[IllegalArgumentException] {
        randomSelect(3, List('a, 'b))
      }
    }
  }

  describe("P23 - alternative") {
    it("should extract random numbers") {
      val fullList: List[Symbol] = List('a, 'b, 'c, 'd, 'f, 'g, 'h)
      val fullSet: Set[Symbol] = fullList.toSet
      val result: Set[Symbol] = randomSelect2(3, List('a, 'b, 'c, 'd, 'f, 'g, 'h)).toSet
      (fullSet intersect result).size should be(result.size)
      result.size should be(3)
    }

    it("should extract nothing") {
      randomSelect2(0, List('a, 'b)) should be(Nil)
    }

    it("should extract full list") {
      randomSelect2(3, List('a, 'b, 'c)).toSet should be(Set('a, 'b, 'c))
    }

    it("should throw exception on illegal argument") {
      intercept[IllegalArgumentException] {
        randomSelect2(-1, List('a, 'b))
      }
      intercept[IllegalArgumentException] {
        randomSelect2(3, List('a, 'b))
      }
    }
  }

  describe("P24") {
    it("should draw random numbers for lotto") {
      val result: Set[Int] = lotto(6, 49)
      result.size should be(6)
      result.filter { x => x > 0 && x <= 49}.size should be(6)
    }

    it("should draw random numbers for bigger range") {
      val result: Set[Int] = lotto(250, 1000)
      result.size should be(250)
      result.filter { x => x > 0 && x <= 1000}.size should be(250)
    }

    it("should draw empty list") {
      lotto(0, 100) should be(Set())
    }

    it("should draw full range") {
      lotto(25, 25).toSet should be((1 to 25).toSet)
    }

    it("should throw exception on illegal argument") {
      intercept[IllegalArgumentException] {
        lotto(-1, 100)
      }

      intercept[IllegalArgumentException] {
        lotto(10, 9)
      }
    }
  }

  describe("P24 - alternative") {
    it("should draw random numbers for lotto") {
      val result: Set[Int] = lotto2(6, 49)
      result.size should be(6)
      result.filter { x => x > 0 && x <= 49}.size should be(6)
    }

    it("should draw random numbers for bigger range") {
      val result: Set[Int] = lotto2(250, 1000)
      result.size should be(250)
      result.filter { x => x > 0 && x <= 1000}.size should be(250)
    }

    it("should draw empty list") {
      lotto2(0, 100) should be(Set())
    }

    it("should draw full range") {
      lotto2(25, 25).toSet should be((1 to 25).toSet)
    }

    it("should throw exception on illegal argument") {
      intercept[IllegalArgumentException] {
        lotto2(-1, 100)
      }

      intercept[IllegalArgumentException] {
        lotto2(10, 9)
      }
    }
  }

  describe("P25") {
    it("should create random permutation") {
      val list: List[Symbol] = List('a, 'b, 'c, 'd, 'e, 'f)
      randomPermute(list).toSet should be(list.toSet)
    }
  }

  describe("P26") {
    it("should create defined combinations") {
      combinations(1, List('a, 'b, 'c)).toSet should be(List(List('a), List('b), List('c)).toSet)
      combinations(2, List('a, 'b, 'c)).toSet should be(List(List('a, 'b), List('a, 'c), List('b, 'c)).toSet)
      combinations(3, List('a, 'b, 'c)).toSet should be(List(List('a, 'b, 'c)).toSet)
      combinations(3, List(1, 2, 3, 4)).toSet should be(List(List(1, 2, 3), List(1, 2, 4), List(1, 3, 4), List(2, 3, 4)).toSet)
      combinations2(1, List('a, 'b, 'c)).toSet should be(List(List('a), List('b), List('c)).toSet)
      combinations2(2, List('a, 'b, 'c)).toSet should be(List(List('a, 'b), List('a, 'c), List('b, 'c)).toSet)
      combinations2(3, List('a, 'b, 'c)).toSet should be(List(List('a, 'b, 'c)).toSet)
      combinations2(3, List(1, 2, 3, 4)).toSet should be(List(List(1, 2, 3), List(1, 2, 4), List(1, 3, 4), List(2, 3, 4)).toSet)
    }

    it("should create correct combinations") {
      val result = combinations(5, List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))
      result foreach (_.toSet.size should be(5))
      (result map (_.toSet)).toSet.size should be(792)

      val result2 = combinations(5, List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))
      result2 foreach (_.toSet.size should be(5))
      (result2 map (_.toSet)).toSet.size should be(792)
    }
  }

  describe("P28") {
    it("should sort according to length") {
      lsort(List(List('a, 'b, 'c), List('d, 'e), List('f, 'g, 'h), List('d, 'e), List('i, 'j, 'k, 'l), List('m, 'n), List('o))) should be(
        List(List('o), List('d, 'e), List('d, 'e), List('m, 'n), List('a, 'b, 'c), List('f, 'g, 'h), List('i, 'j, 'k, 'l))
      )
    }

    it("should sort according to length frequency") {
      lsortFreq(List(List('a, 'b, 'c), List('d, 'e), List('f, 'g, 'h), List('d, 'e), List('i, 'j, 'k, 'l), List('m, 'n), List('o))) should be(
        List(List('i, 'j, 'k, 'l), List('o), List('a, 'b, 'c), List('f, 'g, 'h), List('d, 'e), List('d, 'e), List('m, 'n)))
    }
  }

}
