package intra.cenkar

import intra.cenkar.Logic._
import intra.cenkar.arithmetic.S99Logic._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class LogicSpec extends FunSpec with Matchers {

  describe("P46") {
    it("should return correct truth table") {
      val table = table2((a: Boolean, b: Boolean) => and(a, or(a, b)))
      table.size should be(4)
      table((false, false)) should be(false)
      table((false, true)) should be(false)
      table((true, false)) should be(true)
      table((true, true)) should be(true)
    }
  }

  describe("P47") {
    it("should return correct truth table") {
      val table = table2((a: Boolean, b: Boolean) => a and (a or neg(b)))
      table.size should be(4)
      table((false, false)) should be(false)
      table((false, true)) should be(false)
      table((true, false)) should be(true)
      table((true, true)) should be(true)
    }
  }

  describe("P49") {
    it("should calculate gray code") {
      gray(1) should be(List("0", "1"))
      gray(2) should be(List("00", "01", "11", "10"))
      gray(3) should be(List("000", "001", "011", "010", "110", "111", "101", "100"))
    }
  }

}
