package intra.cenkar

import intra.cenkar.multiwaytree.MTree
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class MTreesSpec extends FunSpec with Matchers {

  describe("P70C") {
    it("should calculate nodes count") {
      MTree('a', Nil).nodeCount should be(1)
      MTree('a', List(MTree('f'))).nodeCount should be(2)
      MTree('a', List(MTree('f'), MTree('g'))).nodeCount should be(3)
      MTree('a', List(MTree('f'), MTree('g', List(MTree('h'))))).nodeCount should be(4)
    }
  }

  describe("P70") {
    it("should create correct to string") {
      MTree('a').toString should be("a^")
      MTree('a', List(MTree('b'))).toString should be("ab^^")
      MTree('a', List(MTree('f', List(MTree('g'))), MTree('c'), MTree('b', List(MTree('d'), MTree('e'))))).toString should be(
        "afg^^c^bd^e^^^")
    }

    it("should parse string") {
      MTree.fromString("a^") should be(MTree('a', Nil))
      MTree.fromString("ab^^") should be(MTree('a', List(MTree('b'))))
      MTree.fromString("abc^^^") should be(MTree('a', List(MTree('b', List(MTree('c'))))))
      MTree.fromString("ab^c^^") should be(MTree('a', List(MTree('b'), MTree('c'))))
      MTree.fromString("ab^c^d^^") should be(MTree('a', List(MTree('b'), MTree('c'), MTree('d'))))
      MTree.fromString("ab^c^de^^^") should be(MTree('a', List(MTree('b'), MTree('c'), MTree('d', List(MTree('e'))))))
      MTree.fromString("afg^^c^^") should be(MTree('a', List(MTree('f', List(MTree('g'))), MTree('c'))))
      MTree.fromString("afg^^c^bd^e^^^") should be(
        MTree('a', List(MTree('f', List(MTree('g'))), MTree('c'), MTree('b', List(MTree('d'), MTree('e'))))))
      MTree.fromString("afgh^^^c^bd^e^^^") should be(
        MTree('a', List(MTree('f', List(MTree('g', List(MTree('h'))))), MTree('c'), MTree('b', List(MTree('d'), MTree('e'))))))
    }
  }

  describe("P71") {
    it("should calculate internal path length") {
      MTree.fromString("a^").internalPathLength should be(0)
      MTree.fromString("ab^^").internalPathLength should be(1)
      MTree.fromString("abc^^^").internalPathLength should be(3)
      MTree.fromString("ab^c^^").internalPathLength should be(2)
      MTree.fromString("afg^^c^be^^^").internalPathLength should be(7)
      MTree.fromString("afg^^c^bd^e^^^").internalPathLength should be(9)
    }
  }

  describe("P72") {
    it("should create post-order traversal") {
      MTree.fromString("a^").postorder should be(List('a'))
      MTree.fromString("ab^^").postorder should be(List('b', 'a'))
      MTree.fromString("abc^^^").postorder should be(List('c', 'b', 'a'))
      MTree.fromString("ab^c^^").postorder should be(List('b', 'c', 'a'))
      MTree.fromString("afg^^c^bd^e^^^").postorder should be(List('g', 'f', 'c', 'd', 'e', 'b', 'a'))
    }
  }

  describe("P73") {
    it("should create lisp like string") {
      MTree('a').lispyTree should be("a")
      MTree('a', List(MTree('b'))).lispyTree should be("(a b)")
      MTree('a', List(MTree('b', List(MTree('c'))))).lispyTree should be("(a (b c))")
      MTree.fromString("afg^^c^bd^e^^^").lispyTree should be("(a (f g) c (b d e))")
    }

    ignore("should parse lisp like string") {
      MTree.fromLispyTree("a") should be(MTree('a'))
      MTree.fromLispyTree("(a b)") should be(MTree('a', List(MTree('b'))))
      MTree.fromLispyTree("(a (b c))") should be(MTree('a', List(MTree('b', List(MTree('c'))))))
      MTree.fromLispyTree("(b d e)") should be(MTree('b', List(MTree('d'), MTree('e'))))
    }
  }
}
