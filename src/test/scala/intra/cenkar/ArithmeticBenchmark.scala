package intra.cenkar

import intra.cenkar.ListsBenchmark.measureTime

object ArithmeticBenchmark extends App {

  println("Totient1: " + measureTime(Arithmetic.totient(10090)))
  println("Totient1: " + measureTime(Arithmetic.totient(10090)))
  println("Totient1: " + measureTime(Arithmetic.totient(10090)))
  println("Totient1: " + measureTime(Arithmetic.totient(10090)))
  println("Totient2: " + measureTime(Arithmetic.totient2(10090)))
  println("Totient2: " + measureTime(Arithmetic.totient2(10090)))
  println("Totient2: " + measureTime(Arithmetic.totient2(10090)))
  println("Totient2: " + measureTime(Arithmetic.totient2(10090)))
  println("Totient - big prime: " + measureTime(Arithmetic.totient2(104395301)))
  println("Totient - big prime: " + measureTime(Arithmetic.totient2(104395301)))
  println("Totient2 - big prime: " + measureTime(Arithmetic.totient2(104395301)))
  println("Totient2 - big prime: " + measureTime(Arithmetic.totient2(104395301)))

}
