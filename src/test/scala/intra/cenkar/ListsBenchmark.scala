package intra.cenkar

object ListsBenchmark extends App {

  println("Lotto1 (10k): " + measureTime(Lists.lotto(10000, 10000)))
  println("Lotto1 (100k): " + measureTime(Lists.lotto(100000, 100000)))
  println("Lotto1 (1M): " + measureTime(Lists.lotto(1000000, 1000000)))
  println("Lotto1 (5M): " + measureTime(Lists.lotto(5000000, 5000000)))
  println("Lotto2 (10k): " + measureTime(Lists.lotto2(10000, 10000)))
  println("Lotto2 (100k): " + measureTime(Lists.lotto2(100000, 100000)))
  println("Lotto2 (1M): " + measureTime(Lists.lotto2(1000000, 1000000)))
  println("Lotto2 (5M): " + measureTime(Lists.lotto2(5000000, 5000000)))

  def measureTime(fun: => Unit): Long = {
    val start = System.currentTimeMillis()
    fun
    System.currentTimeMillis() - start
  }

}
