package intra.cenkar

import intra.cenkar.Arithmetic._
import intra.cenkar.arithmetic.S99Int._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class ArithmeticSpec extends FunSpec with Matchers {

  describe("P31") {
    it("should recognize primes") {
      2.isPrime should be(true)
      3.isPrime should be(true)
      5.isPrime should be(true)
      7.isPrime should be(true)
      66173.isPrime should be(true)
      74353.isPrime should be(true)
      84463.isPrime should be(true)
      90911.isPrime should be(true)
      102881.isPrime should be(true)
      104243.isPrime should be(true)
      104393.isPrime should be(true)
    }

    it("should recognize composites") {
      1.isPrime should be(false)
      4.isPrime should be(false)
      6.isPrime should be(false)
      66175.isPrime should be(false)
      104395.isPrime should be(false)
    }
  }

  describe("P32") {
    it("should calculate gcd") {
      gcd(45, 60) should be(15)
      gcd(154, 564) should be(2)
      gcd(153, 564) should be(3)
      gcd(153, 563) should be(1)
      gcd(158, 564) should be(2)
      gcd(3634, 12972) should be(46)
    }
  }

  describe("P33") {
    it("should calculate if numbers are coprime") {
      6.isCoprimeTo(25) should be(true)
      12.isCoprimeTo(21) should be(false)
      27.isCoprimeTo(16) should be(true)
      35.isCoprimeTo(64) should be(true)
      153.isCoprimeTo(563) should be(true)
    }
  }

  describe("P34") {
    it("should calculate totient") {
      10.totient should be(4)
      100.totient should be(40)
      1000.totient should be(400)
      153.totient should be(96)
      3634.totient should be(1716)
      12972.totient should be(4048)
      104393.totient should be(104392)
    }
  }

  describe("P35") {
    it("should calculate prime factors") {
      315.primeFactors should be(List(3, 3, 5, 7))
      690.primeFactors should be(List(2, 3, 5, 23))
      786.primeFactors should be(List(2, 3, 131))
      786.primeFactors should be(List(2, 3, 131))
      794.primeFactors should be(List(2, 397))
      794.primeFactors should be(List(2, 397))
      787.primeFactors should be(List(787))
      1000.primeFactors should be(List(2, 2, 2, 5, 5, 5))
    }
  }

  describe("P36") {
    it("should create list of prime factors multiplicity") {
      315.primeFactorMultiplicity should be(List((3, 2), (5, 1), (7, 1)))
      787.primeFactorMultiplicity should be(List((787, 1)))
      1000.primeFactorMultiplicity should be(List((2, 3), (5, 3)))
      936.primeFactorMultiplicity should be(List((2, 3), (3, 2), (13, 1)))
    }
  }

  describe("P37") {
    it("should calculate totient") {
      10.totient2 should be(4)
      100.totient2 should be(40)
      1000.totient2 should be(400)
      153.totient2 should be(96)
      3634.totient2 should be(1716)
      12972.totient2 should be(4048)
      104393.totient2 should be(104392)
    }
  }

  describe("P39") {
    it("should list primes in range") {
      listPrimesinRange(7 to 31) should be(List(7, 11, 13, 17, 19, 23, 29, 31))
    }

    ignore("slow one - first milion primes") {
      listPrimesinRange(2 to 15485863) should have length (1000000)
    }
  }

  describe("P40") {
    it("should calculate goldbach conjecture") {
      goldbach(28) should be(5, 23)
    }

    ignore("slow one - up to 25k") {
      ((4 to 25000 by 2).par map (x => (x, goldbach(x))) filter {
        case (n, (c1, c2)) => c1 + c2 != n || (!isPrime(c1) && !isPrime(c2))
      }) should have length (0)
    }
  }

  describe("P41") {
    it("should calculate goldbach conjecture in range") {
      val goldbach = printGoldbachList(9 to 20)
      goldbach.size should be(6)
      goldbach(10) should be((3, 7))
      goldbach(12) should be((5, 7))
      goldbach(14) should be((3, 11))
      goldbach(16) should be((3, 13))
      goldbach(18) should be((5, 13))
      goldbach(20) should be((3, 17))
    }
  }

}
